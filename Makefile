# This is the Top-level Makefile and launcher for Creative Collision Tech
# opensource projects.

# Copyright 2019 Creative Collisions Technology, LLC

all:

check_repo:
	( cd ..; repo forall -p -c git status )

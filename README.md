
Creative Collisions Technology Open Source Codebase
===================================================

This repo manages the software repositories for the Creative Collsions Tech codebase.

Install repo
============

Download the repo tool and make sure it is executable:

	$ mkdir ~/bin
	$ curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
	$ chmod a+x ~/bin/repo

Fetch Git Repositories
======================

1. mkdir repo
2. cd repo

3. repo init -u git@gitlab.com:cct_opensource/cct_open_manifest.git
4. repo sync -d

Build Procedure
===============

Please view the README.md inside of the "cct_main" and the "gravity" project.
